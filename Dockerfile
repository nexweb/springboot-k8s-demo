FROM openjdk:8-jre-alpine
ENV APP_FILE app.jar
ENV APP_HOME /usr/apps
COPY target/*.jar ${APP_HOME}/${APP_FILE}
WORKDIR ${APP_HOME}
EXPOSE 8080

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/.urandom", "-jar", "app.jar"]