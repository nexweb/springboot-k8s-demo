package com.demo.microservices.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/message")
	public String hello() {
		return "Congratulation you successfully deployed your application to kuberntes !";
	}
	
	@GetMapping("/")
	public String welcome() {
		String hostname = System.getenv().getOrDefault("HOSTNAME", "unknown");
	    String message = "Hello world from host";

      	response = "Welcome Springboot to kubernetes from host "+hostname+"\n";

		return response;
	}	
}
